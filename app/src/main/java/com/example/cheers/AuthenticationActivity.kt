package com.example.cheers

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }
    private fun init(){

        auth = Firebase.auth

        logInButton.setOnClickListener { logIn() }

        signUpButton.setOnClickListener {
            val intent = Intent(this,SignUpActivity::class.java)
            startActivity(intent)
        }

        Guest.setOnClickListener {
            val intent = Intent(this,CategoriesActivity::class.java)
            startActivity(intent)
        }
    }

    private fun logIn() {
        val email = logInEmailText.text.toString()
        val password = logInPasswordText.text.toString()

        if (logInEmailText.text.isNotEmpty() and logInPasswordText.text.isNotEmpty()) {

            progressBarLogIn.visibility = View.VISIBLE
            logInButton.isClickable = false

            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->

                        progressBarLogIn.visibility = View.GONE
                        logInButton.isClickable = true

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d( "signInWithEmail:", "success")
                            val user = auth.currentUser

                            val catIntent = Intent(this,CategoriesActivity::class.java)
                            startActivity(catIntent)

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w( "signInWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()
                            // ...
                        }

                        // ...
                    }

        } else{
            Toast.makeText(this,"Please, fill all fields", Toast.LENGTH_SHORT).show()
        }
    }


}