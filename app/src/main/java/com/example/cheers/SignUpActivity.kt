package com.example.cheers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init(){

        auth = Firebase.auth
        signUpButtonReg.setOnClickListener {
            signUp()
        }
    }

    private fun signUp(){

        val email = signUpEmailText.text.toString()
        val password = signUpPasswordText.text.toString()
        val repeatPassword = repeatPasswordText.text.toString()

        if(email.isNotEmpty() and password.isNotEmpty() and repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {

                progressBarReg.visibility = View.VISIBLE
                signUpButtonReg.isClickable = false

                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->

                        progressBarReg.visibility = View.GONE
                        signUpButtonReg.isClickable = true

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("signUp", "createUserWithEmail:success")
                            Toast.makeText(baseContext, "Authentication success.",
                                    Toast.LENGTH_SHORT).show()
                            val user = auth.currentUser
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }

                        // ...
                    }
            } else {
                Toast.makeText(this,"Passwords do not match", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this,"Please, fill all fields", Toast.LENGTH_SHORT).show()
        }
    }
}