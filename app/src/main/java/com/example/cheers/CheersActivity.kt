package com.example.cheers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cheers.*
import kotlin.random.Random

class CheersActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheers)
        init()
    }


    private fun init() {

        val categoryID = intent.getIntExtra("id",1) //ვიღებთ აიდის ღილაკისაგან

        nextButton.setOnClickListener(){
            if(categoryID == 1) {
                val birthdayArr = listOf (
                        getString(R.string.birthday1),
                        getString(R.string.birthday2),
                        getString(R.string.birthday3),
                        getString(R.string.birthday4),
                        getString(R.string.birthday5),
                        getString(R.string.birthday6),
                        getString(R.string.birthday7),
                        getString(R.string.birthday8))
                //  arr.size არის თუ 0-დან რომელ რიცხვამდე გვინდა შემთხვევითი რიცხვის ამოღება
                // arr.size იმ სადღეგრძელოების სიის ზომაა, ანუ რამხენი ელემენტია შიგნით
                val rand = Random.nextInt(birthdayArr.size)
                cheersText.setText(birthdayArr.get(rand))
            }

            if (categoryID == 2) {
                val weddingArr = listOf (
                        getString(R.string.wedding1),
                        getString(R.string.wedding2),
                        getString(R.string.wedding3))
                //  arr.size არის თუ 0-დან რომელ რიცხვამდე გვინდა შემთხვევითი რიცხვის ამოღება
                // arr.size იმ სადღეგრძელოების სიის ზომაა, ანუ რამხენი ელემენტია შიგნით
                val rand = Random.nextInt(weddingArr.size)
                cheersText.setText(weddingArr.get(rand))
            }

            if (categoryID == 3) {
                val qelexiArr = listOf(
                        getString(R.string.qelexi1),
                        getString(R.string.qelexi2),
                        getString(R.string.qelexi3),
                        getString(R.string.qelexi4),
                        getString(R.string.qelexi5),
                        getString(R.string.qelexi6))
                //  arr.size არის თუ 0-დან რომელ რიცხვამდე გვინდა შემთხვევითი რიცხვის ამოღება
                // arr.size იმ სადღეგრძელოების სიის ზომაა, ანუ რამხენი ელემენტია შიგნით
                val rand = Random.nextInt(qelexiArr.size)
                cheersText.setText(qelexiArr.get(rand))
            }
        }

    }


}