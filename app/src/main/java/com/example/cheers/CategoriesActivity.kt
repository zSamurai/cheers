package com.example.cheers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_categories.*

class CategoriesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        init()
    }

    private fun init(){
        birthday.setOnClickListener() {
            val intent = Intent(this, CheersActivity::class.java)
            intent.putExtra("id",1) // გადავეცით ღილაკის აიდი ინტენტს
            startActivity(intent)
        }

        wedding.setOnClickListener() {
            val intent = Intent(this, CheersActivity::class.java)
            intent.putExtra("id",2) // გადავეცით ღილაკის აიდი ინტენტს
            startActivity(intent)
        }

        qelexi.setOnClickListener() {
            val intent = Intent(this, CheersActivity::class.java)
            intent.putExtra("id",3) // გადავეცით ღილაკის აიდი ინტენტს
            startActivity(intent)
        }
    }
}